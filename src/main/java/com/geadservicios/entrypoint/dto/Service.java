package com.geadservicios.entrypoint.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class Service {

    private Long id;
    private String name;
    private String phone;
    private String country;
    private String deparment;
    private String city;
    private String address;
    private String photo;
    private String descriptions;
    private Date date;
    private String comments;
    private String status;

    private Long serviceMade;

}
