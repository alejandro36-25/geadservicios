package com.geadservicios.entrypoinsts;

import com.geadservicios.entrypoint.dto.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "services")
public class ServicesEntryPoint {

    @GetMapping(path= "/", produces="application/json")
    public List<Service> getServices(){
         return List.of(Service.builder().id(1L)
                .name("Reparacion de computadores")
                .phone("3213170958")
                .country("Medellin")
                .deparment("Antioquia")
                .address("Calle 50 #5121")
                .photo("imagen.png")
                .descriptions("Reparo todo tipo de computadores en la ciudad de medellin")
                .comments("Excelente servicio me gusto mucho %--% Totalmente recomendado me encanto.")
                .status("active")
                .serviceMade(10L).build());
    }

}
