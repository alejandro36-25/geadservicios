package com.geadservicios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeadserviciosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeadserviciosApplication.class, args);
	}

}
